#!/bin/bash
#FFMPEG CUT and MERGE 

#GLOBAL VARIABLE
filename="$(date +%s).mp4"
pathout="video/"
list="list.txt"
pathlist=${pathout}${list}


echo "Potong / Gabung Video by Nurza Lib by FFMPEG"
echo "[1] Potong video"
echo "[2] Gabung video"

read -p "Pilih opsi: " opt
case $opt in
1)
ulang=y
while [[ $ulang = [yY] ]]
do
read -p "Masukkan Path file MP4: $(pwd $0)/ " path
mim=$(file "$path" --brief --mime-type)
if [[ ! -e "$path" ]] || [[ ! "$mim" = "video/mp4" ]]; then
echo "File tidak ditemukan atau format video bukan mp4"
read -p "Ulang...? [y/n]: " rpt
ulang=$rpt
if [[ ! $rpt = [yY] ]]; then
exit
fi

else
echo "File ditemukan $path ditemukan dan merupakan video mp4"
ulang=n
fi

done

ulangg=y
while [[ $ulangg = [yY] ]];
do
read -p "Masukkan waktu mulai format [hh:mm:ss]: > " start
read -p "Masukkam waktu akhit format [hh:mm:ss]: > " end
st=$(sed -E 's/:0|://g' <<< "$start")
ed=$(sed -E 's/:0|://g' <<< "$end")
if [[ "$start" =~ [0-9]{1}:[0-9]{2}:[0-9]{2} ]] && [[ "$end" =~ [0-9]{1}:[0-9]{2}:[0-9]{2} ]] && [[ "$st" -lt "$ed" ]]; then
echo "Yess"
echo "Waktu $start - $end"
ulangg=n
else
echo "Format Tidak cocok"
read -p "Ulangi.." ulanx
ulangg=$ulanx
if [[ ! $ulanx = [yY] ]]; then
exit
fi

fi
done


ffmpeg -i "$path" -ss $start -to $end -c copy "$filename"
if [[ $? = 0 ]]; then
echo "Potong video sukses dengan nama ./$filename"
else
echo "Potong Video gagal"
fi

;;
2)
echo "Masukkann file pada sebuah folder contoh ./$pathout (path relative pada script ini menjadi $(dirname $0)/$pathout"
read -p "Enter untuk melanjutkan..."
if [[ ! -e "$pathout" ]]; then
mkdir $pathout
fi
rm $pathlist
OLD=$IFS
IFS=$'\n'
for i in $(find $pathout -iname "*.mp4")
do
echo "Adding .. $i"
echo "file '$i'" >> "${pathlist}"
done
IFS=$OLD
echo "Menggabungkan video dimulai...."
sleep 1
ffmpeg -f concat -i "$pathlist" -c copy "${filename}"
if [[ $? = 0 ]]; then
echo "Penggabungan video Sukses dengan nama ./$filename"
else
echo "Penggabungan video gagal"
fi
;;
*)
echo "Invalid option...!!"
;;
esac


