#!/usr/bin/bash

if [[ "$(whoami)" != "root" ]]; then
echo "This script require root access, because script read database file in system. Please run as Root!!"
exit
fi

# database file location 
dw=/data/data/com.android.providers.downloads/databases/downloads.db

# query extract data from database
sql=$(sqlite3 "$dw" << EOV
.mode list
SELECT _id,title,current_bytes,total_bytes from downloads ORDER by _id DESC LIMIT 10;
EOV
)

# backup ifs
OLD=$IFS
IFS=$'\n'

# loop record sql to process by awk
for i in "$sql"
do
echo "$i" | awk '
BEGIN {
FS="|"
OFS="|"
}
function conver(sat){
# run shell command (getline)
"echo " sat " | numfmt --to=iec" | getline a;
return a;
}

{
if ($3 == $4)
printf "[OK]|";
else
printf "[FAIL]|";
print $1,$2,conver($3),conver($4)
fflush(stdout)
}'

# end of looping
done

# restore ifs
IFS=$OLD
